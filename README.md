# my_4th_sem_project_under_DMK_sir

**web app for trip planning**

this is a webapplication, use to see some ways to travel from source to destination.

with the help of this application user easily able to choose chich way of travel is best for him on the basis of fair, time of travel, comfort etc.

################################################################################

**technologies used :-**

FRONT END :- HTML (design some html pages like input page, output page, login / signup page etc.), JAVA SCRIPT (apply some validation on forms like only present and future date accepted), CSS (apply some styles on html webpages)

BACK END :- PYTHON (language), POSGRES (database) and django as a framework
with the help of django framework i created some files which is MODEL (in model file i write database related code), URL (in url file i write all rendering related code and GET, POST operation code ), and VIEW (in view file i write all paths which are use in web application).

################################################################################

**Prerequisites** :-

(language) latest version of python (the latest version is 3.7)

(database) postgres (database)

(framework) Django

################################################################################

**deployment** :-

first extract .tar.gz file
than reach web app for trip planning folder
than write command
          python3 manage.py runserver

you see output like this -----
--------------------------------------------------------------------------------
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
April 08, 2020 - 07:57:26
Django version 3.0.3, using settings 'web app for trip planning.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.

than open browser and open this link http://127.0.0.1:8000/
--------------------------------------------------------------------------------
than enter source and destination and date of traveling and see all possible vehicales availabel from same source to destination on same date of traveling.

################################################################################

currently i only implement for BUS only. and i am using dummy data which is 01/05/2020 to 07/05/2020 from pune to bhopal and bhopal to pune

################################################################################

**functionality** :-
1. user can search bus from source to destination 
2. user can signup / signin
3. user can book seat in bus
4. user can see his booking
5. user can cancel his booking





